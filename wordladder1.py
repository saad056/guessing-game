import re
# This version of code allows to go from one word to another in least number of steps
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])
# this function returns the length of the common alphabets in item and target e.g. item = "saad" , target = "omar"
#therefore a is common in both words so the length of c is 1

def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]
# build function extracts the alphabets from the words according to the provided pattern and returns them. It also checks
# if the word/words is/are not in the seen dictionary defined as a key and if they are not in the list

def find(word, words, seen, target, path):
  list = []
  maxmatch = 0  #defining a variable to get the maximum number of a matched word length
  maxitem = ""  #defining a variable to get the maximum matched length of word
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
      #this is finding the most common words by comparing it to the word provided and appending them to the list
      #e.g. words = ["saad","omar","saady"] , word = "saa" , so this will return ["saad","saady"]
  if len(list) == 0:
    return False
  list = sorted([(same(w, target), w) for w in list])
  #this basically checks all the words present in list and compares each words with the target word and returns the no. of
  #count for the common alphabets b/w target and each word e.g. For the list defined above and target being set to "sa", it
  #will return: [(2, 'saad'),(2,'saady')]
  
  for (match, item) in list:
    if match >= len(target):
      path.append(item)    #if the length of match is equal to the target word than add the item word in the path list
      return True
    if match >= len(target) - 1:  #However if the matched words length is greater than or equal to one number less than the target word length
      if match == len(target) - 1:
        if match == len(target):   #if the matched words length is equal to the target word length
          path.append(item)        #then append the word item to the path list
          seen[item] = True
          if find(item, words, seen, target, path):
            return True
    if maxmatch <= match:      #if match is greater/equal to max_match number
        maxmatch = match
        maxitem = item

  if match <= maxmatch:       #if the maximum number/length of matched word is greater than or equal to the matched word
    path.append(maxitem)      #append the word to the path list
    seen[item] = True
  if find(item, words, seen, target, path):
    return True
  path.pop()


#the code below prompts the user to enter a start and target word. It inputs those words into the find function
#to match words and append it to the path list and extract the common words for printing



fname = input("Enter dictionary name: ")      #user is asked for input to enter the dictionary name
file = open(fname)    #dictionary is opened and stored in file variable
lines = file.readlines()  #the lines of file are being read and stored in lines
while True:
  start = input("Enter start word:")  #ask the user to enter the start word
  words = []
  for line in lines:
    word = line.rstrip()    #take away the spaces of each line
    if len(word) == len(start):   #if length of the word in dictionary is equal to the start word's length
      words.append(word)       #append the word to the words list
  target = input("Enter target word:")    #asks the user to enter the target word
  break

count = 0
path = [start]
seen = {start : True}
if find(start, words, seen, target, path): #if the match words are found
  print(len(path) - 1, path)   #print the content of path list
else:
  print("No path found")

