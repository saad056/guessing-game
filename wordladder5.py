import re





def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])
# this function returns the length of the common alphabets in item and target e.g. item = "saad" , target = "omar"
#therefore a is common in both words so the length of c is 1



def build(pattern, words, seen, list, donts):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list and word not in donts]
# build function extracts the alphabets from the words according to the provided pattern and returns them. It also checks
# if the word/words is/are not in the seen dictionary defined as a key and if they are not in the list
#The other chnage in the build function is that it takes a list of forbidden words and checks to ensure that the word shouldnt be
#in the forbidden words list


def Forbidden_words():
    f_words = ['side']  #this list will contain all the forbidden words
    # userwords = ""
    # while userwords != '/':    #once the slash is hit on the keyboard, it stops asking for input from the user
    #     userwords = input("Please enter the forbidden word/s, press '/' to stop entering: ")
    #     f_words.append(userwords)  #append all the forbidden words to the f_word list
    # f_words.pop()   #once all the appending is done and the user hits a to end the while loop above, pop the 'a' off the list
    return f_words  #return the list of forbidden words

def find_in_find(word, words, seen, target):  #####This function is exactly the same as find function except this function finds words within the find function
  # (search_word, maximum_match, max_item) = (word, 0, "")
  list = []
  find_word = word
  maxword = ""
  maxword_len = 0
  word_len = len(word)
  target_len = len(target)-1

  for i in range(word_len):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list, forbidden)
      #this is finding the most common words by comparing it to the word provided and appending them to the list
    #e.g. words = ["saad","omar","saady"] , word = "saa" , so this will return ["saad","saady"]
  # if len(list) == 0:
  #   return ("null", 0, "null")
  list = sorted([(same(w, target), w) for w in list], reverse = True)
   #target word and organising the list in a decending order
    #this basically checks all the words present in list and compares each words with the target word and returns the no. of
  #count for the common alphabets b/w target and each word e.g. For the list defined above and target being set to "sa", it
  #will return: [(2, 'saad'),(2,'saady')]
  #Here basically the change is that once the words which have
  #greater length are found and reversed in their order based on their length e.g. if there are words with lengths 3,3,4,5 than
  #the order of these words would be 5,4,3,3
  for (match, item) in list:
    if target_len<=match:
        #Here basically we are subtracting 1 from the target word length everytime the loops
  #     #checks this condition in order to take as less words as possible
      if target_len==match:
          #this is checking if matches length is one character less than the target word
        maxword_len = match
        maxword = item
    if maxword_len < match and maxword < item:
       maxword_len = match        ###getting maximum word and the maximum length
       maxword = item

  return (word, maxword_len, maxword)   #####This gets the maximum length words from within the find function and returns them


def find(word, words, seen, target, path):

  list = []
  #
  # (depth, depth_match, depth_item) = ("", 0, "")
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list, forbidden)
  if len(list) == 0:
    return False



  if select_path == True:      #############short way function
    word_find = []
    foundword = ""
    match_word = ""
    matchword_len = 0
    target_len = len(target)-1
    list = sorted([(same(w, target), w) for w in list], reverse=True)
    for (match, item) in list:
      if target_len<=match:
          if target_len == match:
            path.append(item)
          return True
      seen[item] = True

    for (match, item) in list:
      word_find.append(find_in_find(item, words, seen, target))
    for (find_word, match, item) in word_find:
      if matchword_len<match:
         foundword = find_word
         matchword_len = match
         match_word = item
    path.append(foundword)
    if find(foundword, words, seen, target, path):
      return True
    path.pop()      ################################

  if select_path == False:           ###########Put it in a function
    t_len = len(target)
    maxword = ""
    maxword_len = 0
    target_len = len(target)-1
    list = sorted([(same(w, target), w) for w in list])

    if target in list:
      path.append(target)
      return True
    if match <= maxword_len:
      path.append(maxword)
      seen[item] = True
    for (match, item) in list:
      if match == t_len:
        path.append(item)
        return True
      if maxword_len < match:
         maxword_len = match
         maxword = item
      if target_len<=match:
        if target_len == match:
          if t_len == match:
            path.append(item)
            seen[item] = True
            if find(item, words, seen, target, path):
              return True

    if find(item, words, seen, target, path):
      return True
    path.pop()      #########################




file = open("dictionary.txt")
lines = file.readlines()
while True:
  # start = input("Enter start word:")
  ###In this section the user enters a start and end word
  start = "hide"
  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
  # target = input("Enter target word:")
  target = "seek"
  break

forbidden = Forbidden_words() #initialises the forbidden variable to return the forbidden words list
# print("The Forbidden words are: ")
# print (donts)
select_path = True
count = 0
path = [start]
seen = {start : True}
if find(start, words, seen, target, path):  ####this function finds all the matches and returns the list of words by  appending them to the path
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")

