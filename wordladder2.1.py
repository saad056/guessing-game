import re
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])
# this function returns the length of the common alphabets in item and target e.g. item = "saad" , target = "omar"
#therefore a is common in both words so the length of c is 1

def build(pattern, words, seen, list):
  x = "size"
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list and word != x]
# build function extracts the alphabets from the words according to the provided pattern and returns them. It also checks
# if the word/words is/are not in the seen dictionary defined as a key and if they are not in the list

def find(word, words, seen, target, path):
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
      #this is finding the most common words by comparing it to the word provided and appending them to the list
      #e.g. words = ["saad","omar","saady"] , word = "saa" , so this will return ["saad","saady"]
  if len(list) == 0:
    return False
  list = sorted([(same(w, target), w) for w in list],reverse=True)
  #this basically checks all the words present in list and compares each words with the target word and returns the no. of
  #count for the common alphabets b/w target and each word e.g. For the list defined above and target being set to "sa", it
  #will return: [(2, 'saad'),(2,'saady')]
  #Here basically the change is that once the words which have
  #greater length are found and reversed in their order based on their length e.g. if there are words with lengths 3,3,4,5 than
  #the order of these words would be 5,4,3,3
  for (match, item) in list:
    # lent = len(target) - 1
    if match >= len(target)-1:   #Here basically we are subtracting 2 from the target word length everytime the loops
      #checks this condition in order to take as less words as possible
      if match == len(target)-1:
        #this is checking if matches length is one character less than the target word
        path.append(item)
      return True
    seen[item] = True
      #once the length of target and the match is equal then:
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path):
      return True
    path.pop()

found = True
try:
    fname = input("Enter dictionary name: ")
    file = open(fname)
    lines = file.readlines()
except:
    print('File not found')
    found = False

while found != False:
  start = input("Enter start word:")
  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
  target = input("Enter target word:")
  break
if found != False:
    count = 0
    path = [start]
    seen = {start : True}
    if find(start, words, seen, target, path):
      path.append(target)
      print(len(path) - 1, path)
    else:
      print("No path found")

