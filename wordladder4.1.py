import re



def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])
# this function returns the length of the common alphabets in item and target e.g. item = "saad" , target = "omar"
#therefore a is common in both words so the length of c is 1

def build(pattern, words, seen, list, donts):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list and word not in donts]
# build function extracts the alphabets from the words according to the provided pattern and returns them. It also checks
# if the word/words is/are not in the seen dictionary defined as a key and if they are not in the list
#The other chnage in the build function is that it takes a list of forbidden words and checks to ensure that the word shouldnt be
#in the forbidden words list
def Forbidden_words():
    f_words = []  #this list will contain all the forbidden words
    userwords = ""
    while userwords != '/':    #once the slash is hit on the keyboard, it stops asking for input from the user
        userwords = input("Please enter the forbidden word/s, press '/' to stop entering: ")
        f_words.append(userwords)  #append all the forbidden words to the f_word list
    f_words.pop()   #once all the appending is done and the user hits a to end the while loop above, pop the 'a' off the list
    return f_words  #return the list of forbidden words


def sort_list(word,words,seen,target,donts):
    list = []
    for i in range(len(word)):
     list += build(word[:i] + "." + word[i + 1:], words, seen, list, donts)
          #this is finding the most common words by comparing it to the word provided and appending them to the list
    #e.g. words = ["saad","omar","saady"] , word = "saa" , so this will return ["saad","saady"]
    list = sorted([(same(w, target), w) for w in list], reverse = True)
     #target word and organising the list in a decending order
    #this basically checks all the words present in list and compares each words with the target word and returns the no. of
  #count for the common alphabets b/w target and each word e.g. For the list defined above and target being set to "sa", it
  #will return: [(2, 'saad'),(2,'saady')]
  #Here basically the change is that once the words which have
  #greater length are found and reversed in their order based on their length e.g. if there are words with lengths 3,3,4,5 than
  #the order of these words would be 5,4,3,3
    return list

def find_in_find(word, words, seen, target,forbidden): #####This function is exactly the same as find function except this function finds words within the find function
  maxword = ""
  maxword_len = 0
  match_len = len(target) - 1
  list = []

  sorted_list = sort_list(word,words,seen,target,forbidden)   ##calling the sorted list function

  for (match, item) in sorted_list:
    if match_len <= match:
      if match_len == match:
         maxword_len = match
         maxword = item

    if maxword_len < match and maxword < item:
       maxword_len = match        ###getting maximum word and the maximum length
       maxword = item


  return (word, maxword_len, maxword)   #####This gets the maximum length words from within the find function and returns them

def list_of_words(list,item,words,seen,target):  ###This function is created to return words from find_in_find function and append those words to a list
    words_find = []
    for (match, item) in list:
     words_find.append(find_in_find(item, words, seen, target,forbidden))
    return words_find

def find(word, words, seen, target, path, donts):
  maxword = ""
  maxword_len = 0
  foundword = ""
  match_len = len(target) - 1
  sorted_list = sort_list(word,words,seen,target,donts)

  if len(sorted_list) == 0:
    return False

  for (match, item) in sorted_list:
    if match_len <= match:
         #Here basically we are subtracting 1 from the target word length everytime the loops
      #checks this condition in order to take as less words as possible
      if match_len == match:
           #this is checking if matches length is one character less than the target word
        path.append(item)
      return True
    seen[item] = True

  words_find = list_of_words(sorted_list,item,words,seen,target)   ##this is calling the list_of_words function to make code more cleaner/efficient

  for (words_re, match, item) in words_find:
    if maxword_len < match:
       foundword = words_re
       maxword_len = match
       maxword = item
  path.append(foundword)
  if find(foundword, words, seen, target, path, donts):
    return True
  path.pop()

# fname = input("Enter dictionary name: ")
file = open("dictionary.txt")
lines = file.readlines()
while True:
  # start = input("Enter start word:")
  ###In this section the user enters a start and end word
  start = "hide"
  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
  # target = input("Enter target word:")
  target = "seek"
  break

forbidden = Forbidden_words() #initialises the forbidden variable to return the forbidden words list
# print("The Forbidden words are: ")
# print (donts)

count = 0
path = [start]
seen = {start : True}
if find(start, words, seen, target, path, forbidden):  ####this function finds all the matches and returns the list of words by  appending them to the path
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")

