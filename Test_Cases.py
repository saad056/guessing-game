import unittest
import final

class MyTestCase(unittest.TestCase):
    def test_fname(self):
        res = final.filehandle('d')   ####Checking if file exists
        self.assertEqual(res,False)    ###If file doesn't exists through this error
    def test_1(self):   #This is testing hide to seek conversion
        words = []
        path = []
        seen = {}
        start = "hide"
        target = "seek"
        fname = open("dictionary.txt")
        for each in fname.readlines():
            word = each.rstrip()
            words+=(word)
            break
        res = final.find(start, words, seen, target, path)
        self.assertTrue(res)

    def test_2(self):   #This is testing lead to gold conversion
        words = []
        path = []
        seen = {}
        start = "lead"
        target = "gold"
        fname = open("dictionary.txt")
        for each in fname.readlines():
            word = each.rstrip()
            words+=(word)
            break
        res = final.find(start, words, seen, target, path)
        self.assertTrue(res)

if __name__ == '__main__':
    unittest.main()
#The main function above allows to test the units/functions
